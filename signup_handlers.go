package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

func SignupView(w http.ResponseWriter, r *http.Request) {
	user := User{}
	renderTemplate(w, "signup", &user)
}

func Signup(w http.ResponseWriter, r *http.Request) {
	body := io.LimitReader(r.Body, goBodySize)
	// TODO refactor, call requestAPI
	endpoint := fmt.Sprintf("%s/auth/signup", goAPIURL)
	request, _ := http.NewRequest("POST", endpoint, body)
	request.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		internalErrorResponse(w)
		return
	}
	defer response.Body.Close()

	data, _ := ioutil.ReadAll(response.Body)

	if response.StatusCode == http.StatusCreated {
		if err := setJWTCookie(w, data); err != nil {
			internalErrorResponse(w)
			return
		}
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(response.StatusCode)
	w.Write(data)
}
