
function submitHandler() {
  const form = document.querySelector(".ForgoPasswordForm")

  form.addEventListener("submit", function (evt) {
    evt.preventDefault()

    const email = document.querySelector('#email').value

    const action = this.getAttribute('action')

    fetch(action, {
      method: 'POST',
      body: JSON.stringify({ email })
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data)
      })
      .catch((err) => {
        console.error(err)
      })
  })
}

function ForgotPassword() {

  const init = function () {
    if (window.location.pathname !== '/forgot_password') return

    submitHandler()
  }

  return {
    init
  }
}

export default ForgotPassword()
