import { submitHandler } from './helpers'

function Profile() {

  const init = function () {
    if (window.location.pathname !== '/your_profile') return

    const form = document.querySelector(".ProfileForm")
    const fields = {
      id: document.querySelector('#id'),
      email: document.querySelector('#email'),
      fullname: document.querySelector('#fullname'),
      address: document.querySelector('#address'),
      telephone: document.querySelector('#telephone'),
      password: document.querySelector('#password'),
      confirm_password: document.querySelector('#confirm_password'),
    }
    const cleanFields = {
      password: fields.password,
      confirm_password: fields.confirm_password
    }

    const requestFields = {
      id: fields.id,
      email: fields.email,
      fullname: fields.fullname,
      address: fields.address,
      telephone: fields.telephone,
      password: fields.password,
      confirm_password: fields.confirm_password,
    }

    submitHandler(
      form,
      fields,
      cleanFields,
      requestFields,
      "/your_profile",
      "PUT",
      null,
      "Your profile has been updated!"
    )
  }

  return {
    init
  }
}

export default Profile()
