export function checkStatus(response) {
  if (!response.ok) {   // (response.status < 200 || response.status > 300)
    const error = new Error(response.statusText)
    error.response = response
    throw error
  }
  return response
}

export function parseJSON(response) {
  return response.json()
}

export function handlerErrors(error, cb) {
  const { response } = error
  if (!response) {
    cb(error)
  } else {
    error.status = response.status
    error.statusText = response.statusText
    response.text().then((text) => {
      try {
        const json = JSON.parse(text)
        error.message = json.errors
      } catch (ex) {
        error.message = text
      }
      cb(error)
    })
  }

}

export function displayErrors(fields) {
  return function (error) {
    const { message } = error

    if (typeof message === 'object') {
      Object.keys(fields).forEach((name) => {
        const sibling = fields[name].nextElementSibling
        if (sibling.classList.contains('error-text')) {
          if (message.hasOwnProperty(name)) {
            sibling.classList.add('error-field')
            sibling.innerHTML = message[name]
          } else {
            sibling.classList.remove('error-field')
            sibling.innerHTML = ""
          }
        }
      })
    } else {
      const errorElm = document.querySelector(`.error-msg`)
      if (message && errorElm) {
        errorElm.style.display = "block"
        errorElm.innerHTML = `<p>${message}</p>`
      }
    }
  }
}

export function resetMessages() {
  const successMsg = document.querySelector(`.success-msg`)
  const errorMsg = document.querySelector(`.error-msg`)
  const errorFields = document.querySelectorAll('.error-text')

  if (successMsg) {
    successMsg.style.display = ""
    successMsg.innerHTML = ""
  }

  if (errorMsg) {
    errorMsg.style.display = ""
    errorMsg.innerHTML = ""
  }

  if (errorFields) {
    errorFields.forEach((el) => {
      el.classList.remove('error-field')
      el.innerHTML = ""
    })
  }
}

export function handleSuccess(cleanFields, message) {
  return function (json) {
    Object.keys(cleanFields).forEach((name) => {
      if (typeof cleanFields[name] === 'object'
        && cleanFields[name].hasOwnProperty('value')) {
        cleanFields[name].value = ""
      }
    })

    const el = document.querySelector(`.success-msg`)
    if (el) {
      el.style.display = "block"
      el.innerHTML = `<p>${message}</p>`
    }
  }
}

export function submitHandler(
  form,
  fields,
  cleanFields,
  requestFields,
  action,
  method,
  redirectURL,
  successMsg = "The submission was handled successfully",
) {
  form.addEventListener("submit", function (evt) {
    evt.preventDefault()
    resetMessages()

    let data = {}
    Object.keys(requestFields).forEach((name) => {
      let val = requestFields[name].value
      if (name === 'id') {
        val = parseInt(val)
      }
      data = Object.assign(data, {[name]: val})
    })

    fetch(action, {
      method,
      body: JSON.stringify(data),
      mode: 'cors',
      credentials: 'include'
    })
      .then(checkStatus)
      .then(parseJSON)
      .then((json) => {
        handleSuccess(cleanFields, successMsg)(json)
        if (redirectURL) {
          window.location = redirectURL
          // setTimeout(() => {
          //   window.location = redirectURL
          // }, 400)
        }
      })
      .catch((err) => handlerErrors(err, displayErrors(fields)))
  })
}
