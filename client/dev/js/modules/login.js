import { submitHandler } from './helpers'

function Login() {

  const init = function() {
    if (window.location.pathname !== '/login') return

      const form = document.querySelector(".LoginForm")
      const fields = {
        email: document.querySelector('#email'),
        password: document.querySelector('#password'),
      }

      submitHandler(
        form,
        fields,
        fields,
        fields,
        "/login",
        "POST",
        "/your_profile",
        "Your are now logged in!"
      )
  }

  return {
    init,
  }
}

export default Login()
