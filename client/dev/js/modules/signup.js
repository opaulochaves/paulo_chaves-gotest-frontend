import { submitHandler } from './helpers'

function Signup() {

  const init = function () {
    if (window.location.pathname !== '/signup') return

    const form = document.querySelector(".SignupForm")
    const fields = {
      email: document.querySelector('#email'),
      password: document.querySelector('#password'),
      confirm_password: document.querySelector('#confirm_password'),
    }
    const requestFields = {
      email: fields.email,
      password: fields.password,
      confirm_password: fields.confirm_password,
    }

    submitHandler(
      form,
      fields,
      fields,
      requestFields,
      "/signup",
      "POST",
      "/your_profile",
      "Your are now registered!"
    )
  }

  return {
    init
  }
}

export default Signup()
