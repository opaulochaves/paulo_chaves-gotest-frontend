import {
  Login,
  Signup,
  Profile,
  ForgotPassword } from './modules'

function eventHandler() {
  Login.init()
  Signup.init()
  Profile.init()
  ForgotPassword.init()
}

if (document.readyState === 'complete' || document.readyState !== 'loading') {
  eventHandler();
} else {
  document.addEventListener('DOMContentLoaded', eventHandler);
}
