/**
 * Default Tasks
 */

var gulp = require('gulp');
var runSequence = require('run-sequence');

var mode = require('./helpers/mode');

var defaultTask = function (cb) {
  mode.show();
  mode.production
    ? runSequence(['img', 'css', 'js'], 'size', cb)
    : runSequence(['img', 'css', 'js'], 'watch', cb);
};

gulp.task('default', defaultTask);
