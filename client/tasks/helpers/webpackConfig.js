/**
 * Configuration for Webpack2 bandler.
 */

var webpack = require('webpack');
var path = require('path');

var config = require('../../config.json');
var mode = require('./mode.js');

const JS_DEV = path.resolve(config.root.dev, config.js.dev);
const JS_DIST = path.resolve(config.root.dist, config.js.dist);

module.exports = function (extractVendorLibs) {
  var webpackConfig = {
    context: JS_DEV,
    entry: {
      app: [
        './main.js'
      ]
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
        }
      ]
    },
    output: {
      path: JS_DIST,
      filename: 'main.js',
      publicPath: config.js.dist
    },
    resolve: {
      modules: [JS_DEV, 'node_modules'],
      extensions: config.js.extensions
    },
    plugins: []
  };

  if (mode.production) {
    webpackConfig.plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compress: { warnings: false }
      }),
      new webpack.NoEmitOnErrorsPlugin()
    )
  } else {
    webpackConfig.devtool = 'cheap-module-source-map'
  }

  if (extractVendorLibs) {
    webpackConfig.plugins.push(
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        filename: 'vendor.js'
      })
    );
  }
  return webpackConfig;
};
