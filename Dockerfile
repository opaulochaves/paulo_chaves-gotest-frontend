FROM golang:1.8

# if left blank app will run with dev settings
# to build production image run:
# $ docker build ./paulo_chaves-gotest-frontend --build-arg app_env=production
ARG app_env
ENV APP_ENV $app_env

WORKDIR /go/src/gotest-frontend
COPY . .

RUN go install -v

# if dev setting will use pilu/fresh for code reloading via docker-compose volume sharing with local machine
# if production setting will build binary
CMD if [ ${APP_ENV} = production ]; \
	then \
	gotest-frontend; \
	else \
	go get github.com/pilu/fresh && \
	fresh -c runner.conf; \
	fi
	
EXPOSE 3000
