// From https://jacobmartins.com/2016/02/29/getting-started-with-oauth2-in-go/
package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

func getGoogleOauthConfig() *oauth2.Config {
	return &oauth2.Config{
		RedirectURL:  fmt.Sprintf("%s/oauth2callback", goFrontendURL),
		ClientID:     os.Getenv("GOOGLEKEY"),
		ClientSecret: os.Getenv("GOOGLESECRET"),
		Scopes: []string{"https://www.googleapis.com/auth/userinfo.profile",
			"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint: google.Endpoint,
	}
}

func getOauthStateString(r *http.Request, w http.ResponseWriter, read bool) (string, error) {
	session, err := goSession.Get(r, "GolangTest")
	if err != nil {
		return "", err
	}
	// TODO destroy session on logout and add jwt token into session. The browser will have the session cookie

	if !read {
		session.Values["oAuthStateString"] = "random string"
		err := session.Save(r, w)
		if err != nil {
			return "", err
		}
	}
	return session.Values["oAuthStateString"].(string), nil
}

func GoogleLogin(w http.ResponseWriter, r *http.Request) {
	stateString, err := getOauthStateString(r, w, false)
	if err != nil {
		internalErrorResponse(w)
		return
	}
	url := googleOauthConfig.AuthCodeURL(stateString)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func GoogleCallback(w http.ResponseWriter, r *http.Request) {
	stateString, err := getOauthStateString(r, w, true)
	if err != nil {
		internalErrorResponse(w)
		return
	}

	state := r.FormValue("state")
	if state != stateString {
		log.Printf("invalid oauth state, expected '%s', got '%s'", stateString, state)
		http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
		return
	}

	code := r.FormValue("code")
	token, err := googleOauthConfig.Exchange(oauth2.NoContext, code)
	if err != nil {
		log.Printf("Code exchange failed with '%s'\n", err)
		http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
		return
	}

	data, _ := json.Marshal(map[string]interface{}{
		"code":  code,
		"token": token.AccessToken,
	})

	data, statusCode, err := requestAPI(w, r, "/auth/google", "POST", data)
	log.Println(statusCode, err, string(data))
	if err != nil {
		errorResponse(w, err, statusCode)
		return
	}

	if statusCode != http.StatusOK {
		http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
		return
	}

	if err := setJWTCookie(w, data); err != nil {
		internalErrorResponse(w)
		return
	}

	http.Redirect(w, r, fmt.Sprintf("%s/your_profile", goFrontendURL), http.StatusTemporaryRedirect)
}
