package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"path"
	"strings"
)

func ResetPasswordView(w http.ResponseWriter, r *http.Request) {
	user := User{}
	renderTemplate(w, "reset_password", &user)
}

func ResetPassword(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(`{"data": {"id": "Reset password"}}`))
}

func renderTemplate(w http.ResponseWriter, name string, data interface{}) {
	template := templates.Lookup(name + ".html")
	if template == nil {
		err := fmt.Errorf("Template %q undefined", name)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	template.ExecuteTemplate(w, name, data)
}

func parseTemplates(templatesDir string) (*template.Template, error) {
	var allFiles []string
	files, err := ioutil.ReadDir(templatesDir)
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		filename := file.Name()
		if strings.HasSuffix(filename, ".html") {
			allFiles = append(allFiles, path.Join(templatesDir, filename))
		}
	}

	return template.ParseFiles(allFiles...)
}
