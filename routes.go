// From: https://thenewstack.io/make-a-restful-json-api-go/
package main

import (
	"net/http"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{"Profile", "GET", "/your_profile", ProfileView},
	Route{"Profile", "PUT", "/your_profile", SaveProfile},

	Route{"Login", "GET", "/login", LoginView},
	Route{"Login", "POST", "/login", Login},

	Route{"Signup", "GET", "/signup", SignupView},
	Route{"Signup", "POST", "/signup", Signup},

	Route{"Forgot Password", "GET", "/forgot_password", ForgotPasswordView},
	Route{"Forgot Password", "POST", "/forgot_password", ForgotPassword},

	Route{"Reset Password", "GET", "/reset_password", ResetPasswordView},
	Route{"Reset Password", "POST", "/reset_password", ResetPassword},

	Route{"Google Login", "GET", "/google_login", GoogleLogin},
	Route{"Google Callback", "GET", "/oauth2callback", GoogleCallback},
}
