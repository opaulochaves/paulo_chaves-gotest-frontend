package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
)

func ForgotPasswordView(w http.ResponseWriter, r *http.Request) {
	user := User{}
	renderTemplate(w, "forgot_password", &user)
}

func ForgotPassword(w http.ResponseWriter, r *http.Request) {
	body := io.LimitReader(r.Body, 1048576) // 1MB

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	// TODO get token from cookie and set it as a header in the post request
	apiEndpoint := fmt.Sprintf("%s/auth/forgot", os.Getenv("API_URL"))
	response, err := http.Post(apiEndpoint, "application/json; charset=UTF-8", body)
	if err != nil {
		panic(err)
	}

	data, _ := ioutil.ReadAll(response.Body)
	w.WriteHeader(response.StatusCode)
	w.Write(data)
}
