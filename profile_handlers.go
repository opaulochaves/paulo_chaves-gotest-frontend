package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Profile struct {
	ID              int    `json:"id"`
	Email           string `json:"email"`
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirm_password"`
	Fullname        string `json:"fullname"`
	Address         string `json:"address"`
	Telephone       string `json:"telephone"`
}

func ProfileView(w http.ResponseWriter, r *http.Request) {
	data, statusCode, err := requestAPI(w, r, "/users/profile", "GET", nil)

	if err != nil {
		errorResponse(w, err, statusCode)
		return
	}

	if statusCode == http.StatusOK {
		var profile Profile
		err = json.Unmarshal(data, &profile)
		if err != nil {
			errorResponse(w, err, http.StatusInternalServerError)
			return
		}
		renderTemplate(w, "profile", profile)
		return
	}

	http.Redirect(w, r, fmt.Sprintf("%s/login", goFrontendURL), http.StatusFound)
}

func SaveProfile(w http.ResponseWriter, r *http.Request) {
	data, statusCode, err := requestAPI(w, r, "/users/profile", "PUT", nil)

	if err != nil {
		errorResponse(w, err, statusCode)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(data)
}
