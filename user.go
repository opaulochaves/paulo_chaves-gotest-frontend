package main

type User struct {
	Id        uint   `json:"id"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	Google    string `json:"google"` // keeps the token from Google
	Fullname  string `json:"fullname"`
	Adress    string `json:"address"`
	Telephone string `json:"telephone"`
}

type Users []User
