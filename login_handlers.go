package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

func LoginView(w http.ResponseWriter, r *http.Request) {
	user := User{}
	renderTemplate(w, "login", &user)
}

func Login(w http.ResponseWriter, r *http.Request) {
	body := io.LimitReader(r.Body, goBodySize)

	endpoint := fmt.Sprintf("%s/auth/signin", goAPIURL)
	request, _ := http.NewRequest("POST", endpoint, body)
	request.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		internalErrorResponse(w)
		return
	}

	data, _ := ioutil.ReadAll(response.Body)

	if response.StatusCode == http.StatusOK {
		if err := setJWTCookie(w, data); err != nil {
			internalErrorResponse(w)
			return
		}
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(response.StatusCode)
	w.Write(data)
}
