package main

import (
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/sessions"

	"golang.org/x/oauth2"
)

var templates *template.Template

// global flags
var goAPIURL string
var goFrontendURL string
var goAssetsDIR string
var goPort int
var goTimeout int
var goAppEnv string
var goBodySize int64
var goTokenName string
var goTokenExpires int

var googleOauthConfig *oauth2.Config
var goSession = sessions.NewCookieStore([]byte(os.Getenv("SESSION_SECRET")))

func setEnvVars() {
	if os.Getenv("APP_ENV") != "" {
		goAppEnv = os.Getenv("APP_ENV")
	}
	if os.Getenv("PORT") != "" {
		goPort, _ = strconv.Atoi(os.Getenv("PORT"))
	}
	if os.Getenv("FRONTEND_URL") != "" {
		goFrontendURL = os.Getenv("FRONTEND_URL")
	}
	if os.Getenv("API_URL") != "" {
		goAPIURL = os.Getenv("API_URL")
	}
}

func main() {
	var err error

	flag.StringVar(&goAPIURL, "api-url", "http://api:8080/api", "Base absolute path to the REST API endpoints")
	flag.StringVar(&goFrontendURL, "frontend-url", "http://localhost:3000", "Base absolute path to the frontend")
	flag.StringVar(&goAssetsDIR, "dir", ".", "The directory to serve files from. Defaults to the current dir")
	flag.IntVar(&goPort, "port", 3000, "Specify the port to listen to.")
	flag.IntVar(&goTimeout, "timeout", 15, "Seconds for timeout. Good practice: enforce timeouts for servers you create!")
	flag.StringVar(&goAppEnv, "env", "development", "Set run mode of the app. production or development")
	flag.StringVar(&goTokenName, "token-name", "jwt_token", "Name of the cookie to store the JWT token")
	flag.IntVar(&goTokenExpires, "token-expires", 604800, "The max-age of the token in seconds. Default to one week, 604800 seconds")
	flag.Int64Var(&goBodySize, "body-size", 1048576, "Limit the size of the body in the request. Default to 1048576 bytes")
	flag.Parse()

	setEnvVars()
	googleOauthConfig = getGoogleOauthConfig()

	templates, err = parseTemplates("./templates")
	if err != nil {
		panic(err)
	}

	router := NewRouter()
	router.PathPrefix("/assets").Handler(http.StripPrefix("/", http.FileServer(http.Dir(goAssetsDIR))))

	srv := &http.Server{
		Handler:      router,
		Addr:         fmt.Sprintf(":%d", goPort),
		WriteTimeout: time.Duration(goTimeout) * time.Second,
		ReadTimeout:  time.Duration(goTimeout) * time.Second,
	}

	if goAppEnv == "production" {
		log.Println("Running frontend in production mode on port", goPort)
	} else {
		log.Println("Running frontend in dev mode on port", goPort)
	}

	if err := srv.ListenAndServe(); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
