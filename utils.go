package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

func setJWTCookie(w http.ResponseWriter, data []byte) error {
	var jsonResponse map[string]interface{}
	err := json.Unmarshal(data, &jsonResponse)
	if err != nil {
		return err
	}

	cookie := http.Cookie{
		Name:     goTokenName,
		Value:    jsonResponse["token"].(string),
		MaxAge:   goTokenExpires,
		Path:     "/",
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
	return nil
}

func GetTokenFromCookie(r *http.Request) string {
	cookie, _ := r.Cookie(goTokenName)
	if cookie != nil {
		return cookie.Value
	}
	return ""
}

func errorResponse(w http.ResponseWriter, msg interface{}, status int) {
	data, err := json.Marshal(map[string]interface{}{
		"status": status,
		"errors": msg,
	})
	if err != nil {
		panic(err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(data)
}

func internalErrorResponse(w http.ResponseWriter) {
	errorResponse(w, "Internal Error", http.StatusInternalServerError)
}

// requestAPI makes a request to the API
// return response data, status and a posible error
func requestAPI(w http.ResponseWriter, r *http.Request, resource, method string, requestData []byte) ([]byte, int, error) {
	var body io.Reader
	if requestData == nil {
		body = io.LimitReader(r.Body, goBodySize)
	} else {
		body = bytes.NewReader(requestData)
	}

	endpoint := fmt.Sprintf("%s%s", goAPIURL, resource)
	request, _ := http.NewRequest(method, endpoint, body)
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", GetTokenFromCookie(r)))
	client := &http.Client{}
	response, err := client.Do(request)

	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	data, _ := ioutil.ReadAll(response.Body)
	defer response.Body.Close()

	return data, response.StatusCode, nil
}
